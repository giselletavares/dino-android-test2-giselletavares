package com.example.tappyspaceship01;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    final static String TAG="DINO-RAINBOWS";
    final static String[] ITEMS_ARRAY = new String[]{ "candy64", "poop64", "rainbow64" };
    final static int[] ITEMS_POSITIONS = new int[]{100, 400, 700, 1000};
    final static int LOW_SPEED = 25;
    final static int HIGH_SPEED = 50;

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;
    String imageName;
    int imageId;
    int indexItemImageName;
    int indexItemPosition;
    Bitmap poop;

    boolean screenTouched;



    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------
    Random mRandom = new Random();
    ArrayList<Item> mItemArrayList;
    int itemRandomSpeed;

    // ----------------------------
    // ## SPRITES
    // ----------------------------
    Player dino;
    Item item;

    int laneXPosition;
    int laneYPosition;

    int laneWidth;
    int laneHeight;


    // represent the TOP LEFT CORNER OF THE GRAPHIC

    // ----------------------------
    // ## GAME STATS
    // ----------------------------
    int score;
    int lives;


    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;

        this.laneXPosition = 20;
        this.laneYPosition = 300;

        this.laneWidth = screenWidth - 400;
        this.laneHeight = 50;

        this.poop = BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable.poop64);

        this.printScreenInfo();

        this.spawnPlayer();

        this.mItemArrayList = new ArrayList<>();
//        for (int i = 0; i < 4; i ++){
            this.spawnItems();
//        }
    }

    private void printScreenInfo() {
        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }

    private void spawnPlayer() {
        dino = new Player(this.getContext(), this.screenWidth - 250, 400);
    }

    private void spawnItems() {
        this.itemRandomSpeed = mRandom.nextInt(HIGH_SPEED - LOW_SPEED) + LOW_SPEED;
        this.indexItemImageName = mRandom.nextInt(ITEMS_ARRAY.length);
        this.indexItemPosition = mRandom.nextInt(ITEMS_POSITIONS.length);

        item = new Item(this.getContext(), 30, 300);

        this.imageName = ITEMS_ARRAY[indexItemImageName];
        this.imageId = this.getContext().getResources().getIdentifier(this.imageName, "drawable", this.getContext().getPackageName());
        item.setImage(BitmapFactory.decodeResource(getContext().getResources(), imageId));

        item.setyPosition(ITEMS_POSITIONS[indexItemPosition]);
        item.setHitbox(new Rect(this.item.getxPosition(),
                this.item.getyPosition(),
                this.item.getxPosition() + this.item.getImage().getWidth(),
                this.item.getyPosition() + this.item.getImage().getHeight()));

        this.mItemArrayList.add(item);
    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        this.score = 0;
        this.lives = 3;
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    public void gameOver(){
        if (this.holder.getSurface().isValid()) {

            this.canvas = this.holder.lockCanvas();
            this.canvas.drawColor(Color.argb(255, 0, 0, 0));
            paintbrush.setColor(Color.WHITE);
            paintbrush.setTextSize(60);
            canvas.drawText("Score: " + this.score, screenWidth/2 - 120, screenHeight - 800, paintbrush);
            paintbrush.setTextSize(200);
            canvas.drawText("GAME OVER", screenWidth/2 - 550, screenHeight / 4, paintbrush);
            paintbrush.setColor(Color.YELLOW);
            paintbrush.setTextSize(100);
            canvas.drawText("TAP to PLAY AGAIN", screenWidth / 5, screenHeight - 500, paintbrush);
            this.holder.unlockCanvasAndPost(canvas);

            this.pauseGame();
        }
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    public void updatePositions() {

        if (this.fingerAction == "up") {
            if(this.dino.getyPosition() > 0 && this.screenTouched) {
                this.dino.setyPosition(this.dino.getyPosition() - 300);
                this.screenTouched = false;

                dino.setHitbox(new Rect(dino.getxPosition(),
                        dino.getyPosition(),
                        dino.getxPosition() + dino.getImage().getWidth(),
                        dino.getyPosition() + dino.getImage().getHeight()));
            }

            Log.d(TAG, "dino up pos: " + this.dino.getyPosition());

            if(this.dino.getyPosition() <= 100) {
                this.dino.setyPosition(100);
                dino.setHitbox(new Rect(dino.getxPosition(),
                        dino.getyPosition(),
                        dino.getxPosition() + dino.getImage().getWidth(),
                        dino.getyPosition() + dino.getImage().getHeight()));
                Log.d(TAG, "MOUSEDOWN - HIT THE ROOF - this.playerYPosition:  " + this.dino.getyPosition());
            }
        }

        if (this.fingerAction == "down") {
            if(this.dino.getyPosition() < screenHeight - this.dino.getImage().getHeight() && this.screenTouched) {
                this.dino.setyPosition(this.dino.getyPosition() + 300);
                this.screenTouched = false;

                dino.setHitbox(new Rect(dino.getxPosition(),
                        dino.getyPosition(),
                        dino.getxPosition() + dino.getImage().getWidth(),
                        dino.getyPosition() + dino.getImage().getHeight()));
            }
            Log.d(TAG, "dino down pos: " + this.dino.getyPosition());

            if(this.dino.getyPosition() >= 1000){
                this.dino.setyPosition(1000);
                dino.setHitbox(new Rect(dino.getxPosition(),
                        dino.getyPosition(),
                        dino.getxPosition() + dino.getImage().getWidth(),
                        dino.getyPosition() + dino.getImage().getHeight()));
                Log.d(TAG, "MOUSEUP - HIT THE FLOOR - this.playerYPosition: " + this.dino.getyPosition());
            }
        }

        for(Item item: this.mItemArrayList){
            item.setxPosition(item.getxPosition() + 15);
            item.setHitbox(new Rect(item.getxPosition(),
                    item.getyPosition(),
                    item.getxPosition() + item.getImage().getWidth(),
                    item.getyPosition() + item.getImage().getHeight()));

            if(item.getxPosition() >= (this.screenWidth + item.getImage().getWidth())) {
                // restart the enemy in the starting position
                this.mItemArrayList.remove(item);
                spawnItems();
                item.setHitbox(new Rect(item.getxPosition(),
                        item.getyPosition(),
                        item.getxPosition() + item.getImage().getWidth(),
                        item.getyPosition() + item.getImage().getHeight()));
            }

            if(this.dino.getHitbox().intersect(item.getHitbox()) == true && this.poop.sameAs(item.getImage())){
                this.mItemArrayList.remove(item);
                this.lives -= 1;
                spawnItems();
            } else if (this.dino.getHitbox().intersect(item.getHitbox()) == true && !this.poop.sameAs(item.getImage())){
                this.mItemArrayList.remove(item);
                this.score += 1;
                spawnItems();
            }

            if(this.lives < 1){
                this.gameOver();
            }
        }
    }

    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,255,255,255));
            paintbrush.setColor(Color.WHITE);


            // DRAW THE PLAYER HITBOX
            // ------------------------
            // 1. change the paintbrush settings so we can see the hitbox
            paintbrush.setColor(Color.BLUE);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(5);

            // draw player graphic on screen
            canvas.drawBitmap(this.dino.getImage(), this.dino.getxPosition(), this.dino.getyPosition(), paintbrush);
            // draw the player's hitbox
            canvas.drawRect(this.dino.getHitbox(), paintbrush);

            // draw the enemy graphic on the screen
            canvas.drawBitmap(this.item.getImage(), this.item.getxPosition(), this.item.getyPosition(), paintbrush);
            // 2. draw the enemy's hitbox
            canvas.drawRect(this.item.getHitbox(), paintbrush);

            // draw the lanes
            this.canvas.drawRect(
                    laneXPosition,
                    laneYPosition,
                    laneXPosition + laneWidth,
                    laneYPosition + laneHeight,
                    paintbrush);

            this.canvas.drawRect(
                    laneXPosition,
                    laneYPosition + 300,
                    laneXPosition + laneWidth,
                    (laneYPosition + 300) + laneHeight,
                    paintbrush);

            this.canvas.drawRect(
                    laneXPosition,
                    laneYPosition + 600,
                    laneXPosition + laneWidth,
                    (laneYPosition + 600) + laneHeight,
                    paintbrush);

            this.canvas.drawRect(
                    laneXPosition,
                    laneYPosition + 900,
                    laneXPosition + laneWidth,
                    (laneYPosition + 900) + laneHeight,
                    paintbrush);



            // the score and lives
            paintbrush.setColor(Color.BLACK);
            paintbrush.setTextSize(60);
            canvas.drawText("SCORE: " + this.score, 100, 100, paintbrush);
            canvas.drawText("LIVES: " + this.lives, 500, 100, paintbrush);

            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(5);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------


    String fingerAction = "";

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();

        if (userAction == MotionEvent.ACTION_DOWN) {
            if(gameIsRunning){
                float fingerYposition = event.getY();

                float middleScreen = this.screenHeight / 2;

                if(fingerYposition < middleScreen){
                    fingerAction = "up";
                } else {
                    fingerAction = "down";
                }
                this.screenTouched = true;
            } else {
                startGame();
            }
        }

        return true;
    }
}
